#ifndef _TOKENS
#define _TOKENS

#include <limits.h>

typedef enum {T_WORD, T_BAR, T_GT, T_LT, T_NEW, T_EOF, T_ERROR, T_SQUOTE, T_DQUOTE, T_EMPTY} TTokenEnum;

typedef struct Token
{
	TTokenEnum token;
	char word[CHAR_MAX];
} TToken;

typedef struct TokenArray
{	
	TToken *tokens;
	size_t size;
	size_t used;
} TTokenArray;

void tok_create(TTokenArray *array, size_t size);
void tok_add(TTokenArray *array, TToken token);
void tok_free(TTokenArray *array);
char *tok_print(TToken *token);
void tok_print_array();

#endif
