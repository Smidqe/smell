#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "tokens.h"

//globals
#define SMELL_BL_BUFF 1024
#define SMELL_MAX_COMMANDS 32
#define SMELL_ARGUMENT_MAX 64
#define SMELL_ALARM 30

//global variables
char cur_path[500];

TTokenArray *getTokens(char* line);

void sighandler(int sig)
{
	switch (sig)
	{
		case SIGALRM:
		{
			printf("Auto logout\n");
			exit(0);
		}
			
		default: break;
	}
	return;
}

//only holds cd, everything else should come from system
int checkBuiltin(int argc, char *args[])
{
	char *path;
		
	//compare the first argument to cd, if it's not then return -1 (there are no function called that builtin)
	if (strcmp(args[0], "cd") == 0)
	{
		//check if we have an argument (it's > 1 because the first argument is the actual command
		if (argc > 1)
			path = args[1];
		
		//check if there is no arguments, and get the HOME path from enviroment variables, if it fails make the path to be . (current)
		if (argc == 1)
			if ((path = getenv("HOME")) == NULL)
				path = ".";
	
		//change the directory, and if it fails, just print the error, otherwise update our global path var.
		if (chdir(path) == -1)
			fprintf(stderr, "%s: Invalid directory\n", path);
		else
			getcwd(cur_path, sizeof(cur_path));
	}
	else
		return -1;
	//return 0 to indicate a success.
	return 0;
}

void redirect(int srcfd, char *srcf, int dstfd, char *dstf)
{	
	//check the source file if it has been set	
	if (srcf[0] != '\0')
		srcfd = open(srcf, O_RDONLY, 0);

	dup2(srcfd, STDIN_FILENO);
	
	if (srcfd != STDIN_FILENO)
		close(srcfd);

	//check if our destination file is set and open it for writing
	if (dstf[0] != '\0')
		dstfd = open(dstf, O_WRONLY | O_CREAT | O_TRUNC, S_IRWXU);			

	//copy the destionation filedescriptor to STDOUT_FILENO
	dup2(dstfd, STDOUT_FILENO);
	
	//close the destination filedescriptor if it is different (meaning failure)
	if (dstfd != STDOUT_FILENO)
		close(dstfd);
	
	return;
}

void replace(char *word, char remove, char replace)
{
	for (int i = 0; i < strlen(word); i++)
		if (word[i] == remove)
			word[i] = replace;
	
}

pid_t start(int argc, char *args[], int srcfd, char *srcf, int dstfd, char *dstf)
{
	pid_t pid;
	char *cmd, *path;	

	if (checkBuiltin(argc, args) == 0)
		return 0;

	//fork this so we have a child process
	switch(pid = fork())
	{
		case -1: 
		{
			fprintf(stderr, "Failed to create a new process");
			return 0;
		}
		
		case 0:
		{
			//redirect io from source to destination (if set)
			redirect(srcfd, srcf, dstfd, dstf);
			
			//check if we have a path (custom program) or a system call
			cmd = strchr(args[0], '/');
			if (cmd == NULL)
				cmd = args[0];
			else
				cmd++; //moves the pointer by 1, effectively removing . if it exists

			//set the path 
			path = args[0];

			//set the command to be the first one (execvp requires this, so that it knows what program to run, incase there are multiple programs)
			args[0] = cmd;
			
			execvp(path, args);
			fprintf(stderr, "Could not execute: %s\n", cmd);
			_exit(EXIT_FAILURE);
		}
	}
	
	//close the filedescriptors
	if (srcfd > STDIN_FILENO)
		close(srcfd);

	if (dstfd > STDOUT_FILENO)
		close(dstfd);
	
	return pid;
}
/*
int cnt_pipes(TTokenArray *a)
{
	int result = 0;

	for (int i = 0; i < a->used; i++)
		if (a->tokens[i].token == T_BAR)
			result++;

	return result; 
}*/

TTokenEnum handle(TTokenArray *a, pid_t *wpid)
{
	TToken current;
	int argpos, srcfd, dstfd, pid;
	int pipes[2];
	int usepipes;
	char *args[SMELL_ARGUMENT_MAX];
	char srcf[500] = "", dstf[500] = "";

	argpos = 0;
	//set our source and destionation filedescriptors to stdin and stdout
	srcfd = STDIN_FILENO;
	dstfd = STDOUT_FILENO;
	for (int i = 0; i < a->used; i++)
	{
		//get the current token, helps since you don't need to write so much
		current = a->tokens[i];
		
		switch(current.token)
		{
			case T_SQUOTE:
			case T_DQUOTE:
			case T_WORD:
			{
				if (argpos > SMELL_ARGUMENT_MAX - 1)
					continue;
				//if we have reached our limit or it fails
				if ((args[argpos] = malloc(strlen(current.word) + 1)) == NULL)
					continue;
				//do not put the output as argument, otherwise it will be printed aswell
		
				if (a->tokens[i - 1].token != T_GT || i == 0)
				{
					strcpy(args[argpos], current.word);
					argpos++;
				}
				
				continue;
			}
			//handle < and >
			case T_LT:
			case T_GT:
			{
				if (i == 0)
					break;	
				
				if (dstfd != STDOUT_FILENO && usepipes == 0)
					break;

				TToken tok;

				tok = a->tokens[i + (current.token == T_LT ? -1 : 1)];				
				
				if (tok.token != T_WORD)
				{
					if (tok.token == current.token && current.token == T_GT)
						printf(">> is not implemented\n");
					else
						printf("Next or previous argument is not a word\n");
					
					break;
				}
				else
					strcpy(current.token == T_LT ? srcf : dstf, tok.word);
				
				//change the srcfd or dstfd to -1 (indicating that we are not using stdin or stdout)
				if (current.token == T_LT)
					srcfd = -1;
				else
					dstfd = -1;
					
				continue;
			}	
			
		
			//handle cases for |, and newline
			case T_BAR:
			case T_NEW:
			{
				//check if we have an invalid pipe at start
				if (current.token == T_BAR && i == 0)
					break;	

				args[argpos] = NULL;
			
				//check if we have a pipe
				if (current.token == T_BAR)
				{
					//there is no need to set it more than once if there is multiple pipes
					if (usepipes == 0)
						usepipes = 1;
					
					//create pipes
					pipe(pipes);
			
					//set the destination fd to the write part of the pipe
					dstfd = pipes[1];
				}
				else
				{		
				

					//if we have set pipe and have hit end of the line, set destination back to stdout
					if (dstfd != STDOUT_FILENO)
						dup2(dstfd, STDOUT_FILENO);
				}
				//call the function.
				pid = start(argpos, args, srcfd, srcf, dstfd, dstf);	
				
				//close the write part of the pipe
				if (current.token == T_BAR)
				{
					close(pipes[1]);
					srcfd = pipes[0];
				}		
	
				//free the arguments
				while (--argpos >= 0)
					free(args[argpos]);
						
				argpos = 0;

				//close pipe and reset the source and destination filedescriptors
				if (current.token != T_BAR)
				{
					if (usepipes == 1)
					{
						if (srcfd != STDIN_FILENO)
							dup2(srcfd, STDIN_FILENO);

						if (dstfd != STDOUT_FILENO)
							dup2(dstfd, STDOUT_FILENO);

						close(pipes[0]);
						close(pipes[1]);
					}
			
					*wpid = pid;
					return current.token;
				}
				else
				{
					//printf("Not exiting, we don't have a T_NEW\n");
					continue;
				}
			}
			case T_EOF: exit(EXIT_SUCCESS);
			case T_ERROR: break;
			case T_EMPTY: continue;
		}
	}		
		
	return T_ERROR;
}

TTokenEnum getToken(char c)
{			
	switch(c)
	{
		case '|': return T_BAR;
		case '<': return T_LT;
		case '\n': return T_NEW;
		case '\0': return T_EOF;	
		case ' ':
		case '\t': 
			return T_EMPTY;
	
		case '\"': return T_DQUOTE;
		case '\'': return T_SQUOTE;
		case '>': return T_GT; //for the cases where there is a >> do we even need a separate thing to check it?
			
		default:
			return T_WORD;	
	}
}

TTokenArray *parse(char *line)
{
	char word[SMELL_BL_BUFF];
	int index = 0;
	int word_index = 0;
	int inquote = 0;
	char qc;
	TToken *cont = NULL;
	TTokenArray *tokens = NULL;
	
	tokens = malloc(sizeof(TTokenArray));	
	tok_create(tokens, 1);
	
	//create the dynamic token array
	while (line[index] != '\0')
	{
		cont = malloc(sizeof(TToken));
		cont->token = getToken(line[index]);
	
		//check if we have stumbled upon a word
		if (cont->token == T_WORD || cont->token == T_SQUOTE || cont->token == T_DQUOTE)
		{
			//check if we have gotten a quote
			if (cont->token == T_SQUOTE || cont->token == T_DQUOTE)
			{
				inquote = 1;	
				qc = line[index];
			}

			//clear the word since we already have another one.
			memset(&word[0], 0, sizeof(word));
			word_index = 0;	

			//
			while (getToken(line[index]) == T_WORD || inquote)
			{
				index++;
	
				//check if we have either a null or we are in a quote and we haven't stumbled upon a ending quote
				if (line[index] == '\0' || (inquote && line[index] == qc))
				{
					word[word_index] = line[index - 1];		
					break;
				}
			
				//check if our previous character was a quote (TODO: add single quote)	
				if (inquote && (line[index - 1] == qc))
					continue;

				word[word_index] = line[index - 1];
				word_index++;
			}
			

			inquote = 0;
			strcpy(cont->word, word);
			//printf("%s\n", word);
		}	

		//check if we encountered a word, don't add more to index if we did
		if (cont->token != T_WORD)
			index++;

		//we don't need empty spaces, only the tokens are what we need.
		if (cont->token != T_EMPTY)
			tok_add(tokens, *cont);
	} 	

	//we are at the end
	if (line[index] == '\0')
	{
		cont = malloc(sizeof(TToken));
		if (index == 0)
			cont->token = T_WORD;
		else
			cont->token = T_EOF;
		
		tok_add(tokens, *cont);
		
		if (index == 0)
		{
			cont = malloc(sizeof(TToken));
			cont->token = T_EOF;
			tok_add(tokens, *cont);
		}
	}	
	
	cont = NULL;
	return tokens;
}

char *smell_read_line(void)
{
	int bsize = SMELL_BL_BUFF;
	int position = 0;
	char *buffer = malloc(sizeof(char) * bsize);
	int ch;

	if (!buffer)
	{
		fprintf(stderr, "Failed to allocate memory to the buffer");
		exit(EXIT_FAILURE);
	}

	while (1)
	{
		ch = getchar();
		
		//check if we are at the end
		if (ch == EOF || ch == '\n')
		{
			buffer[position] = ch;
			buffer[position + 1] = '\0';
			break;
		}
		else
			buffer[position] = ch;

		position++;
		
		//reallocate more memory to buffer if current is not enough
		if (position >= bsize)
		{
			bsize += SMELL_BL_BUFF;
			buffer = realloc(buffer, bsize);
			
			if (!buffer)
			{
				fprintf(stderr, "smell: reallocation error");
				exit(EXIT_FAILURE);
			}
		}
	}

	return buffer;	
}

void smell_mainloop(void)
{
	pid_t pid, wpid;
	int status;
	char *line;

	TTokenEnum token;
	TTokenArray *tokens;
	
	signal(SIGALRM, sighandler);
	signal(SIGINT, sighandler);
	
	//alarm(SMELL_ALARM);

	getcwd(cur_path, sizeof(cur_path));
	while (1)
	{
		//get the current directory from location.c
		//printf("@" + location.current + " > ")
		printf("%s > ", cur_path);
		line = smell_read_line();	
		
		//if we only have newline (empty string)
		if (strcmp(line, "\n") == 0)
		{
			printf("Continuing\n");
			continue;
		}
		//if we have exit, could also be builtin, but meh...
		if (strcmp(line, "exit\n") == 0)
		{
			printf("Exiting program\n");
			free(line);
			exit(EXIT_SUCCESS);
		}	
		
		//parse the line
		tokens = parse(line);

		//handle the parsed line
		token = handle(tokens, &pid);
	
		//free the tokens
		tok_free(tokens);	
		free(tokens);
		free(line);	
		
		//if we got an error just skip the waiting to finish
		if (token == T_ERROR)
			continue;
	
		//check if the child process is still running
		if (pid != 0)	
			do
			{	
				wpid = waitpid(-1, &status, 0); 
			} while (wpid != pid);
	}
}


int main(int argc, char* argv[])
{
	smell_mainloop();

	return EXIT_SUCCESS;
}

