/*
	Compilation of things for the token arrays
*/
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <stdio.h>

typedef enum {T_WORD, T_BAR, T_GT, T_LT, T_NEW, T_EOF, T_ERROR, T_SQUOTE, T_DQUOTE, T_EMPTY} TTokenEnum;

typedef struct Token
{
	TTokenEnum token;
	char word[CHAR_MAX];
	
} TToken;

typedef struct TokenArray
{
	TToken *tokens;
	size_t size;
	size_t used;

} TTokenArray;

void tok_create(TTokenArray *array, size_t size)
{
	array->tokens = (TToken *) malloc(size * sizeof(TToken));
	array->used = 0;
	array->size = size;	
}

void tok_add(TTokenArray *array, TToken token)
{
	if (array->used == array->size)
	{
		array->size += 1;
		array->tokens = (TToken *) realloc(array->tokens, array->size * sizeof(TToken)); 
	}
	
	array->tokens[array->used++] = token;
}

void tok_free(TTokenArray *array)
{
	free(array->tokens);

	array->tokens = NULL;
	array->used = array->size = 0;
}

char *tok_print(TToken *token)
{
	switch(token->token)
	{
		case T_WORD: return "T_WORD";
		case T_BAR: return "T_BAR";
		case T_GT: return "T_GT";
		case T_LT: return "T_LT";
		case T_NEW: return "T_NEW";
		case T_EOF: return "T_EOF";
		case T_ERROR: return "T_ERROR";
		case T_EMPTY: return "T_EMPTY";
		case T_SQUOTE: return "T_SQUOTE";
		case T_DQUOTE: return "T_DQUOTE";
	
	}
	
	return NULL;
}

void tok_print_array(TTokenArray *a)
{
	
	for (int i = 0; i < a->used; i++)
	{
		if ((a->tokens[i]).token == T_WORD)
			printf("%s, <%s>\n", tok_print(&a->tokens[i]), (a->tokens[i]).word);
		else
			printf("%s\n", tok_print(&a->tokens[i]));
	}
}
